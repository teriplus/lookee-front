export function zeroPad(num, places) {
    return String(num).padStart(places, "0");
}

export function handleDaykey(year, month, day) {
    if (
        year == "" ||
        year == undefined ||
        month == "" ||
        month == undefined ||
        day == "" ||
        day == undefined
    ) {
        return null;
    }
    return year + zeroPad(month, 2) + zeroPad(day, 2);
}

export function handleTime(hour, min) {
    return zeroPad(hour, 2) + zeroPad(min, 2);
}

export function daykeyToArray(daykey) {
    return [
        daykey.substr(0, 4),
        daykey.substr(4, 2),
        daykey.substr(6, 2)
    ]
}

export function timekeyToArray(timekey) {
    return [
        timekey.substr(0, 2),
        timekey.substr(1, 2),
    ]
}