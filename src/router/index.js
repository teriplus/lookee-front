import Vue from 'vue'
import VueRouter from 'vue-router'

import panel from './panel'
import web from './web'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: panel.concat(web)
})

export default router
