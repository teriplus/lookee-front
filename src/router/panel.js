import adminAccess from '../middlewares/adminAccess'

import Dashboard from "../views/panel/Dashboard.vue"
import Users from "../views/panel/Users.vue"
import Events from "../views/panel/Events.vue"
import Payments from "../views/panel/Payments.vue"
import Statistics from "../views/panel/Statistics.vue"
import Roles from "../views/panel/Roles.vue"
import Cache from "../views/panel/Cache.vue"



import Navbar from "../components/panel/Navbar.vue"

const panel = [
    {
        path: '/admin', name: 'panel.Dashboard', components: {
            default: Dashboard, viewPanel: Navbar,
        }
    },
    // users
    {
        path: '/admin/users', name: 'panel.Users', components: {
            default: Users, viewPanel: Navbar,
        }
    },
    {
        path: '/admin/users/:page', name: 'panel.Users', components: {
            default: Users, viewPanel: Navbar,
        }
    },
    // events
    {
        path: '/admin/events', name: 'panel.Events', components: {
            default: Events, viewPanel: Navbar,
        }
    },
    {
        path: '/admin/events/:page', name: 'panel.Events', components: {
            default: Events, viewPanel: Navbar,
        }
    },
    // payments
    {
        path: '/admin/payments', name: 'panel.Payments', components: {
            default: Payments, viewPanel: Navbar,
        }
    },
    {
        path: '/admin/payments/:page', name: 'panel.Payments', components: {
            default: Payments, viewPanel: Navbar,
        }
    },
    // statistics
    {
        path: '/admin/statistics', name: 'panel.Statistics', components: {
            default: Statistics, viewPanel: Navbar,
        }
    },
    {
        path: '/admin/statistics/:page', name: 'panel.Statistics', components: {
            default: Statistics, viewPanel: Navbar,
        }
    },
    // roles
    {
        path: '/admin/roles', name: 'panel.Roles', components: {
            default: Roles, viewPanel: Navbar,
        }
    },
    {
        path: '/admin/roles/:page', name: 'panel.Roles', components: {
            default: Roles, viewPanel: Navbar,
        }
    },
    // System
    {
        path: '/admin/cache', name: 'panel.Cache', components: {
            default: Cache, viewPanel: Navbar,
        }
    },
]

// ~Middlewares
for (const route of panel) {
    route.beforeEnter = (to, from, next) => {
        adminAccess().then(r => {
            r.isAdmin ? next() : next({ name: 'web.Page404' })
        })
    }
}

export default panel