import userAccess from '../middlewares/userAccess'

import Home from '../views/web/Home.vue'
import Login from '../views/web/Login.vue'
import Register from '../views/web/Register.vue'
import Logout from '../views/web/Logout.vue'
import Profile from '../views/web/Profile.vue'
import CreateEvent from '../views/web/CreateEvent.vue'
import EditEvent from '../views/web/EditEvent.vue'
import Event from '../views/web/Event.vue'
import Page404 from '../views/web/Page404.vue'
import YourEvents from '../views/web/YourEvents.vue'
import EventsYouJoined from '../views/web/EventsYouJoined.vue'

const web = [
    { path: '/', name: 'web.Home', component: Home },
    { path: '/events/:page', name: 'web.Events.Page', component: Home },
    { path: '/events', name: 'web.Events.master', component: Home },

    { path: '/logout', name: 'web.Logout', component: Logout },
    { path: '/e/:id', name: 'web.Event', component: Event },
    { path: '*', name: 'web.Page404', component: Page404 },
]

const users = [
    { path: '/profile', name: 'web.Profile', component: Profile },
    { path: '/new/event', name: 'web.CreateEvent', component: CreateEvent },
    { path: '/your/events', name: 'web.YourEvents', component: YourEvents },
    { path: '/e/edit/:id', name: 'web.EditEvent', component: EditEvent },
    { path: '/events/youjoined', name: 'web.EventsYouJoined', component: EventsYouJoined },
]

const guests = [
    { path: '/login', name: 'web.Login', component: Login },
    { path: '/register', name: 'web.Register', component: Register },
]

// ~Middlewares
for (const route of users) {
    route.beforeEnter = (to, from, next) => {
        userAccess().then(r => {
            r.isLogin ? next() : next({
                name: 'web.Login',
                params: {
                    nextUrlName: to.name,
                    nextUrl: to.fullPath,
                    niceRedirect: true
                }
            })
        })
    }
}

for (const route of guests) {
    route.beforeEnter = (to, from, next) => {
        userAccess().then(r => { r.isLogin ? next('/') : next() })
    }
}


export default web
    .concat(users)
    .concat(guests)