
const config = {
    // Develop
    app_name: "lookee",
    site_base: "http://localhost:8080",
    base_api: "http://localhost:8000/v1"

    // Product
    // app_name: "http://lookee",
    // site_base: "http://lookee.ir",
    // base_api: "http://lookee.ir/api/v1"
}

export default config