import Axios from "axios";
import config from "../config";
import VueCookies from "vue-cookies";

const adminAccess = () => {
  return Axios.post(config.base_api + "/admin/access", null, {
    headers: {
      token: VueCookies.get("token")
    },
  })
    .then((response) => {
      return {
        isAdmin: true,
        accessList: response.data.access,
        name: response.data.name
      }
    })
    .catch((e) => {
      return {
        isAdmin: false
      }
    });
}

export default adminAccess