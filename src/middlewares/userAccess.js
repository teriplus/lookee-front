import Axios from "axios";
import config from "../config";
import VueCookies from "vue-cookies";

const userAccess = () => {
  return Axios.post(config.base_api + "/check/auth", null, {
    headers: {
      token: VueCookies.get("token")
    },
  })
    .then((response) => {
      return {
        isLogin: true,
        name: response.data.name
      }
    })
    .catch((e) => {
      return {
        isLogin: false
      }
    });
}

export default userAccess