import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    events: {
      total: String,
      paginate: String,
      page: String
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
